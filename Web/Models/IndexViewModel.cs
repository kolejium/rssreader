﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class IndexViewModel
    {
        public IEnumerable<ArticleModel> Articles { get; set; }

        public PageInfo PageInfo { get; set; }
    }
}
