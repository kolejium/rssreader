﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class ArticleModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DatePublish { get; set; }

        public string Url { get; set; }
    }
}
