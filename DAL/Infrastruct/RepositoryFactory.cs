﻿using DAL.Contexts;
using Ninject;
using Ninject.Parameters;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Infrastruct
{
    public sealed class RepositoryFactory
    {
        private IKernel Kernel { get; set; }
        private EFContext context;

        public RepositoryFactory(EFContext context)
        {
            this.context = context;
            Config();
        }

        private void Config()
        {
            Kernel = new StandardKernel(new NinjectConfigModule());
        }

        public T Get<T>()
        {
            return Kernel.Get<T>(new[] {
                new ConstructorArgument("context", context) });
        }
    }
}
