﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Infrastruct
{
    internal class NinjectConfigModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IChannelRepository>().To<ChannelRepository>();
            Bind<IItemRepository>().To<ItemRepository>();
        }
    }
}
