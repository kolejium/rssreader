﻿using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Infrastruct
{
    public class UnitOfWork
    {
        private EFContext context;

        private RepositoryFactory repositoryFactory;

        public RepositoryFactory Factory => repositoryFactory;

        public UnitOfWork()
        {
            context = new EFContext();
            repositoryFactory = new RepositoryFactory(context);
            
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
