﻿using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class ItemRepository : Repository<Item>, IItemRepository
    {
        public ItemRepository(EFContext context) : base(context)
        {

        }

        public Item Find(string title, DateTime datePublish)
        {
            return dbSet.Find(title, datePublish);
        }
    }
}
