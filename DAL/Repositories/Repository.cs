﻿using DAL.Contexts;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected EFContext context;
        protected DbSet<T> dbSet;

        public Repository(EFContext context)
        {
            this.context = context;
            dbSet = context.Set<T>();
        }

        public void Create(T item)
        {
            dbSet.Add(item);
        }

        public T Find(string key)
        {
            return dbSet.Find(key);
        }

        public IEnumerable<T> Get()
        {
            return dbSet;
        }

        public IEnumerable<T> Get(Func<T, bool> predicate)
        {
            return dbSet.Where(predicate);
        }

        public void Remove(T item)
        {
            dbSet.Remove(item);
        }

        public void Update(T item)
        {
            dbSet.Update(item);
        }
    }
}
