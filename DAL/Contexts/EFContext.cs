﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Ninject.Activation;
using Ninject.Parameters;
using Ninject.Planning.Targets;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Contexts
{
    public class EFContext : DbContext
    {
        public DbSet<Channel> Channels { get; set; }
        public DbSet<Item> Items { get; set; }

        public EFContext()
        {
            // Database.EnsureDeleted(); // TODO: Delete/Comment
            //Database.EnsureCreated(); // Create DB if not exists
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=RSSReaderDatabase;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Channel>().HasKey(x => x.Url);
            modelBuilder.Entity<Item>().HasKey(x => new { x.Title, x.DatePublish });
            modelBuilder.Entity<Item>().HasOne(i => i.Channel).WithMany(c => c.Items).IsRequired().HasForeignKey("ChannelUrl").OnDelete(DeleteBehavior.Cascade);
        }
    }
}
