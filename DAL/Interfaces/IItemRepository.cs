﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces
{
    public interface IItemRepository : IRepository<Item>
    {
        Item Find(string title, DateTime datePublish);
    }
}
