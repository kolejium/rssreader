﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class Foreign : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_Channels_Url",
                table: "Items");

            migrationBuilder.DropIndex(
                name: "IX_Items_Url",
                table: "Items");

            migrationBuilder.DropPrimaryKey(
                name: "UrlChannel",
                table: "Channels");

            migrationBuilder.AlterColumn<string>(
                name: "Url",
                table: "Items",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "ChannelUrl",
                table: "Items",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Channels",
                table: "Channels",
                column: "Url");

            migrationBuilder.CreateIndex(
                name: "IX_Items_ChannelUrl",
                table: "Items",
                column: "ChannelUrl");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_Channels_ChannelUrl",
                table: "Items",
                column: "ChannelUrl",
                principalTable: "Channels",
                principalColumn: "Url",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_Channels_ChannelUrl",
                table: "Items");

            migrationBuilder.DropIndex(
                name: "IX_Items_ChannelUrl",
                table: "Items");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Channels",
                table: "Channels");

            migrationBuilder.DropColumn(
                name: "ChannelUrl",
                table: "Items");

            migrationBuilder.AlterColumn<string>(
                name: "Url",
                table: "Items",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "UrlChannel",
                table: "Channels",
                column: "Url");

            migrationBuilder.CreateIndex(
                name: "IX_Items_Url",
                table: "Items",
                column: "Url");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_Channels_Url",
                table: "Items",
                column: "Url",
                principalTable: "Channels",
                principalColumn: "Url",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
