﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class Item
    {
        public string Title { get; set; }
        public DateTime DatePublish { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }

        public bool Read { get; set; }
        
        public string ChannelUrl { get; set; }

        public virtual Channel Channel { get; set; }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType())
                return false;
            Item item = (obj as Item);
            if (item.Title == this.Title && item.DatePublish == this.DatePublish
                && item.Description == this.Description && item.Url == this.Url
                && item.Channel == this.Channel)
                return true;
            return false;
        }

        public override string ToString()
        {
            return $"\n\r{Title} | {DatePublish} | \n\r{Description}\n\r{Url} - {Read}\n\r Source: {Channel.Url}";
        }
    }
}
