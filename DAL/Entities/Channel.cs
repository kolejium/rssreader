﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Entities
{
    public class Channel
    {
        public string Url { get; set; }
        public string Name { get; set; }

        public virtual List<Item> Items { get; set; }

        public Channel()
        {
            Items = new List<Item>();
        }

        public Channel(string url, string name, List<Item> items)
        {
            Url = url;
            Name = name;
            Items = items;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType())
                return false;
            Channel channel = (Channel)obj;
            return this.Name == channel.Name && this.Url == channel.Url;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
