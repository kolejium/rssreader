﻿using BLL.DTOs;
using BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static RSSService service;
        static List<ChannelDTO> lastChannels;
        static List<ItemDTO> lastItems;

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            service = new RSSService();
            while (true)
            {
                Menu();
            }
        }

        static void Menu()
        {
            Console.WriteLine("\n -- Menu -- ");
            Console.WriteLine("1. Read all news from all channels \n\r2. Save only new news \n\r3. Show stats by channel \n\r -- -- -- --");
            Console.WriteLine("For help input '?' or 'help'\n\r");
            Input(Console.ReadLine());
        }

        static void Input(string input)
        {
            input.ToLower();
            switch (input)
            {
                case "1":
                    {
                        Input("--d -c -read");
                    }
                    break;
                case "2":
                    {
                        Input("--l -i");
                    }
                    break;
                case "3":
                    {
                        Input("--d -c -stats");
                    }
                    break;
                case "?":
                case "help":
                    {
                        Console.WriteLine(" -- Help Menu --");
                        YellowWriteLine("\n How to input command: \n");
                        YellowWriteLine("[command] [parameters] => --a -c http://habrahabr.ru/rss/ " +
                            "or --a -c http://habrahabr.ru/rss/ http://www.interfax.by/news/feed");
                        YellowWriteLine("[command] [digit parameters] => --r -c 0 1 2");
                        GreenWrite("--r -c 0 1 => Good ");
                        YellowWriteLine(" --r -c 0, 1 => ERROR!");
                        GreenWriteLine(" -- List Commands -- ");
                        GreenWriteLine("clear - clear console");
                        GreenWriteLine("clear res - clear cache res");
                        GreenWriteLine("--a -c [parameters] - add new channel (can use many parameters)," +
                            " example: --a -c http://habrahabr.ru/rss/");
                        GreenWriteLine("--r -c [parameters] - remove channel (can use many parameters)," +
                            " (can use cache parameters), example: --r -c http://habrahabr.ru/rss");
                        GreenWriteLine("--d -c - display and save channels to cache");
                        GreenWriteLine("--d -c -stats [parametes|empty] - display stats channels" +
                            " (can use many parameters), (can use cache parameters)," +
                            " (can use no parameters), example: --d -c -stats 0");
                        GreenWriteLine("--d -i [parameters|empty|-l] - display news from channel" +
                            " (can use many parameters), (can use cache parameters)," +
                            " (can use no parameters)," +
                            " example: --d -i http://habrahabr.ru/rss or" +
                            " --d -i 0 or --d -i -l or --d -i");
                        GreenWriteLine("--l -i [parameters|empty] - load news (can use many parameters)," +
                            " (can use cache parameters), (can use no parameters)" +
                            " example: --l -i http://habrahabr.ru/rss or" +
                            " --l -i 0 or --l -i");
                        GreenWriteLine("--d -c -read [parameters|empty] - read news (can use many parameters)," +
                            " (can use cache parameters), (can use no parameters)" +
                            " example: --d -c -read http://habrahabr.ru/rss or" +
                            " --d -c -read 0 or --d -c");
                        GreenWrite("--d -c -unread - unread all news! ");
                        YellowWriteLine("This test function.");
                    }
                    break;
                case "clear":
                    {
                        Console.Clear();
                    }
                    break;
                case "clear res":
                    {
                        lastChannels = null;
                        lastItems = null;
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine("Res was nullable");
                        Console.ResetColor();
                    }
                    break;
                case string value when new Regex(@"--a -c ((http|https):\/\/\S+(\s*))+").IsMatch(input):
                    {
                        service.AddChannel(GetArgs("--a -c", input));
                    }
                    break;
                case string value when new Regex(@"--r -c (((http|https):\/\/\S+(\s*))+|(\d+(\s*))+)").IsMatch(input):
                    {
                        int num;
                        string[] args = GetArgs("--r -c", input);
                        if (int.TryParse(args[0], out num))
                        {
                            List<ChannelDTO> channels = new List<ChannelDTO>();
                            foreach (var item in args)
                            {
                                try
                                {
                                    num = Convert.ToInt32(item);
                                    channels.Add(lastChannels[num]);
                                }
                                catch
                                {

                                }
                            }
                            service.RemoveChannel(channels.ToArray());
                            return;
                        }
                        service.RemoveChannel(args);
                    }
                    break;
                case "--d -c":
                    {
                        lastChannels = service.GetChannels().ToList();
                        foreach (var item in lastChannels)
                        {
                            Console.WriteLine(item.ToString());
                        }
                    }
                    break;
                case string value when new Regex(@"--d -c -stats($|\s)(((http|https):\/\/\S+(\s*))+|(\d+(\s*))+)?").IsMatch(input):
                    {
                        string[] args = GetArgs("--d -c -stats", input);
                        if (args.Length > 0 && IsDigit(args[0]))
                        {
                            List<ChannelDTO> channels = GetTByIndex<ChannelDTO>(args, lastChannels);
                            Console.WriteLine(service.GetStatsByChannel(channels.ToArray()));
                        }
                        else
                            Console.WriteLine(service.GetStatsByChannel(args));
                    }
                    break;
                case "--d -i -l":
                    {
                        lastItems.ForEach(x => Console.WriteLine(x.ToString()));
                    }
                    break;
                case string value when new Regex(@"--d -i($|\s)(((http|https):\/\/\S+(\s*))+|(\d+(\s*))+)?").IsMatch(input):
                    {
                        string[] args = GetArgs("--d -i", input);
                        if (args.Length > 0 && IsDigit(args[0]))
                        {
                            List<ChannelDTO> channels = GetTByIndex<ChannelDTO>(args, lastChannels);
                            lastItems = service.LoadNewsFromDatabase(channels.ToArray()).ToList();
                        }
                        else
                            lastItems = service.LoadNewsFromChannel(args).ToList();
                        Input("--d -i -l");
                    }
                    break;
                case string value when new Regex(@"--l -i($|\s)(((http|https):\/\/\S+(\s*))+|(\d+(\s*))+)?").IsMatch(input):
                    {
                        string[] args = GetArgs("--l -i", input);
                        if (args.Length > 0 && IsDigit(args[0]))
                        {
                            List<ChannelDTO> channels = GetTByIndex<ChannelDTO>(args, lastChannels);
                            service.AddNewNewsToDatabase(channels.ToArray());
                        }
                        else
                            service.AddNewNewsToDatabase(args);
                    }
                    break;
                case string value when new Regex(@"--d -c -read($|\s)(((http|https):\/\/\S+(\s*))+|(\d+(\s*))+)?").IsMatch(input):
                    {
                        string[] args = GetArgs("--d -c -read", input);
                        if (args.Length > 0 && IsDigit(args[0]))
                        {
                            List<ChannelDTO> channels = GetTByIndex<ChannelDTO>(args, lastChannels);
                            service.ReadAllNewsFromChannel(channels.ToArray());
                            Input("--d -i" + input.Replace("--d -c -read", ""));
                        }
                        else
                        {
                            service.ReadAllNewsFromChannel(args);
                            Input("--d -i");
                        }
                    }
                    break;
                case "--d -c -unread":
                    {
                        service.UnreadAllNewsFromAllChannels();
                    }
                    break;
                //case "display news from channel":
                //case "display -i -c":
                //case "--d -i -c":
                //    {
                //        Console.WriteLine("Input url channel or Enter for all channels. Input last (-l), for display cache news if they exists");
                //        string i = Console.ReadLine();
                //        switch (i)
                //        {
                //            case "-l":
                //                {

                //                }
                //        }
                //        Channel channel = service.GetChannelByUrl(Console.ReadLine());
                //        foreach (var item in service.LoadNewsFromDatabase(channel))
                //        {
                //            Console.WriteLine(item.ToString());
                //        }
                //    }
                //    break;
                //case "load news from channel(s)":
                //case "load -i -c":
                //case "--l -i -c":
                //    {
                //        Console.WriteLine("if you want load from all channels, input ENTER");
                //        string[] vs = GetArgs();
                //        if (vs.Length != 0)
                //            service.LoadNewsFromChannel(GetArgs());
                //        else
                //            service.LoadNewsFromChannel();
                //    }
                //    break;
                default:
                    {
                        Console.WriteLine(Environment.NewLine);
                        Console.ResetColor();
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.Write("- ");
                        Console.Write("Error query: ");
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(input);
                        Console.ResetColor();
                    }
                    break;
            }
        }

        static void GreenWrite(string str)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(str);
            Console.ResetColor();
        }

        static void GreenWriteLine(string str)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(str);
            Console.ResetColor();
        }

        static void YellowWrite(string str)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(str);
            Console.ResetColor();
        }

        static void YellowWriteLine(string str)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(str);
            Console.ResetColor();
        }

        static string[] GetArgs(string command, string str)
        {
            str = str.Replace(command, "");
            return str.Split().Where(x => x != "").ToArray();
        }

        static bool IsDigit(string source)
        {
            int num;
            if (source != "" && int.TryParse(source, out num))
                return true;
            return false;
        }
        
        static List<T> GetTByIndex<T>(string[] args, List<T> source)
        {
            int index;
            List<T> list = new List<T>();
            try
            {
                foreach (var item in args)
                {
                    if (int.TryParse(item, out index))
                        list.Add(source[index]);
                }
            }
            catch { }
            return list;
        }

    }
}
