﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataProvider.Entities
{
    public class Channel
    {
        [Key]
        public string Url { get; set; }
        public string Name { get; set; }

        public List<Item> Items { get; set; }
    }
}
