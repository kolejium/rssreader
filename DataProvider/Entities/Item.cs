﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataProvider.Entities
{
    public class Item
    {
        public string Title { get; set; }
        public DateTime DatePublish { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }

        public bool Read { get; set; }

        public string ChannelUrl { get; set; }
        public Channel Channel { get; set; }
    }
}
