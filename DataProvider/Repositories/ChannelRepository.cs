﻿using AutoMapper;
using DataInterfaces.DTOs;
using DataInterfaces.Interfaces;
using DataProvider.Contexts;
using DataProvider.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataProvider.Repositories
{
    /// <summary>
    /// Repository to work with the Channel table
    /// </summary>
    public class ChannelRepository : IChannelRepository
    {
        private EFContext _context;

        public ChannelRepository(EFContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Create new Channel
        /// </summary>
        /// <param name="item">Class instance ChannelDTO</param>
        public void Create(ChannelDTO item)
        {
            _context.Add(item);
        }

        /// <summary>
        /// Delete Channel
        /// </summary>
        /// <param name="item">Class instance ChannelDTO</param>
        public void Delete(ChannelDTO item)
        {
            _context.Remove(item);
        }

        /// <summary>
        /// Get all orders from the table Channel
        /// </summary>
        /// <returns>Enumerable ChannelDTO</returns>
        public IEnumerable<ChannelDTO> Get()
        {
            return Mapper.Map<IEnumerable<Channel>, IEnumerable<ChannelDTO>>(_context.Channels);
        }

        /// <summary>
        /// Select order from the table Channel by primary key
        /// </summary>
        /// <param name="key">primary key</param>
        /// <returns>ChannelDTO</returns>
        public ChannelDTO Get(string key)
        {
            return Mapper.Map<Channel, ChannelDTO>(_context.Channels.Find(key));
        }

        /// <summary>
        /// Update order in the table Channel
        /// </summary>
        /// <param name="item">Updating ChannelDTO</param>
        public void Update(ChannelDTO item)
        {
            _context.Update(item);
        }
    }
}
