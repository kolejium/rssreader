﻿using DataInterfaces.DTOs;
using DataInterfaces.Interfaces;
using DataProvider.Contexts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataProvider.Repositories
{
    public class ItemRepository : IItemRepository
    {
        private EFContext _context;

        public ItemRepository(EFContext context)
        {
            _context = context;
        }

        public void Create(ItemDTO item)
        {
            throw new NotImplementedException();
        }

        public void Delete(ItemDTO item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ItemDTO> Get()
        {
            throw new NotImplementedException();
        }

        public ItemDTO Get(string key)
        {
            throw new NotImplementedException();
        }

        public void Update(ItemDTO item)
        {
            throw new NotImplementedException();
        }
    }
}
