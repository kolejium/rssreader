﻿using DataProvider.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataProvider.Contexts
{
    public class EFContext : DbContext
    {
        public DbSet<Channel> Channels { get; set; }
        public DbSet<Item> Items { get; set; }

        public EFContext()
        {
            Database.EnsureCreated(); // Create DB if not exists
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=RSSReaderDatabase;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Item>().HasKey(x => new { x.Title, x.DatePublish });
            modelBuilder.Entity<Item>().HasOne(i => i.Channel).WithMany(c => c.Items).HasForeignKey(fk => fk.ChannelUrl);
        }
    }
}
