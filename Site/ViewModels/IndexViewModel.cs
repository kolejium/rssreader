﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site.ViewModels
{
    public class IndexViewModel
    {
        public SelectList RenderChannels { get; set; }
        public string SelectedChannel { get; set; }

        public string Sort { get; set; }

        public List<ArticleModel> Articles { get; set; }

        public PageInfo PageInfo { get; set; }
    }
}
