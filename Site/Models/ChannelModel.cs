﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site.Models
{
    public class ChannelModel
    {
        public string Url { get; set; }
        public string Name { get; set; }
    }
}
