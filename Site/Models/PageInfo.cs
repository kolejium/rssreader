﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site.Models
{
    public class PageInfo
    {
        public int PageNumber { get; set; } // номер текущей страницы
        public int PageSize { get; set; } // кол-во объектов на странице
        public int TotalItems { get; set; } // всего объектов
        public int TotalPages  // всего страниц
        {
            get { return TotalItems/PageSize; }
        }

        public PageInfo(int number, int size, int count)
        {
            PageNumber = number;
            PageSize = size;
            TotalItems = count;
        }
    }
}
