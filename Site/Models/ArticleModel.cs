﻿using Microsoft.AspNetCore.Html;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site.Models
{
    public class ArticleModel
    {
        public string Title { get; set; }
        public HtmlString Description { get; set; }
        public DateTime DatePublish { get; set; }

        public string Url { get; set; }
    }
}
