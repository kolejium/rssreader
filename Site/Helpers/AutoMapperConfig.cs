﻿using AutoMapper;
using BLL.DTOs;
using Microsoft.AspNetCore.Html;
using Site.Models;

namespace Site.Helpers
{
    public static class AutoMapperConfig
    {
        private static Mapper mapper;
        public static Mapper Mapper
        {
            get
            {
                Config();
                return new Mapper(Configuration);
            }
        }

        public static MapperConfiguration Configuration { get; set; }

        static AutoMapperConfig()
        {
            Config();
        }

        private static void Config()
        {
            Configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ChannelDTO, ChannelModel>();
                cfg.CreateMap<ItemDTO, ArticleModel>()
                .ForMember(x => x.Url, z => z.MapFrom(src => src.Channel.Url))
                .ForMember(x => x.Description, z => z.MapFrom(src => new HtmlString(src.Description.Replace("<img", "<img style='width: 100%'"))));
            });
        }
    }
}
