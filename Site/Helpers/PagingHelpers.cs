﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Site.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace Site.Helpers
{
    public static class PagingHelpers
    {
        public static HtmlString PageLinks(this IHtmlHelper html, PageInfo pageInfo, Func<int, string> pageUrl)
        {
            StringWriter writer = new StringWriter();
            int left = pageInfo.PageNumber - 3 > 0 ? pageInfo.PageNumber - 3 : 1;
            int right = pageInfo.PageNumber + 3 <= pageInfo.TotalPages ? pageInfo.PageNumber + 3 : pageInfo.TotalPages;
            for (int i = left; i <= right; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml.Append(i.ToString());
                if (i == pageInfo.PageNumber)
                    tag.AddCssClass("ButtonGreen");
                else
                    tag.AddCssClass("ButtonRed");
                tag.AddCssClass("Button");
                tag.WriteTo(writer, HtmlEncoder.Default);
            }
            return new HtmlString(writer.ToString());
        }
    }
}
