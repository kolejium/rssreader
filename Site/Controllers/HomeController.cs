﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTOs;
using BLL.Services;
//using BLL.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Site.Helpers;
using Site.Models;
using Site.ViewModels;

namespace Site.Controllers
{
    public class HomeController : Controller
    {
        RSSService service;
        IMapper mapper;

        public HomeController()
        {
            service = new RSSService();
            mapper = AutoMapperConfig.Mapper;
        }

        public IActionResult Index(string sort = "Date", string selectedChannel = "All", int page = 1)
        {
            SelectList sl = new SelectList(mapper.Map<IEnumerable<ChannelDTO>, IEnumerable<ChannelDTO>>(service.GetChannels()), "Url", "Name");
            List<ArticleModel> articles = null;
            articles = selectedChannel == "All"
                ?
                mapper.Map<IEnumerable<ItemDTO>, IEnumerable<ArticleModel>>(service.LoadNewsFromDatabase()).Skip((page - 1) * 10).Take(10).ToList()
                : mapper.Map<IEnumerable<ItemDTO>, IEnumerable<ArticleModel>>(service.LoadNewsFromDatabase(service.GetChannelByUrl(selectedChannel))).Skip((page - 1) * 10).Take(10).ToList();
            PageInfo pageInfo = new PageInfo(page, 10, mapper.Map<IEnumerable<ItemDTO>, IEnumerable<ArticleModel>>(service.LoadNewsFromDatabase()).Count());
            IndexViewModel view = new IndexViewModel()
            {
                RenderChannels = sl,
                PageInfo = pageInfo,
                Articles = articles,
                SelectedChannel = selectedChannel,
                Sort = "Date"
            };
            return View(view);
        }

        [HttpPost]
        public IActionResult Index(string sort = "Date", string selectedChannel = "All")
        {
            return Index(sort, selectedChannel, 1);
        }

        IndexViewModel GetIndexViewModel(IndexViewModel model)
        {
            model.RenderChannels = new SelectList(mapper.Map<IEnumerable<ChannelDTO>, IEnumerable<ChannelDTO>>(service.GetChannels()), "Url", "Name");
            if (model.SelectedChannel == null)
            {
                model.Articles = mapper.Map<IEnumerable<ItemDTO>, IEnumerable<ArticleModel>>(service.LoadNewsFromDatabase()).ToList();
                model.SelectedChannel = "All";
            }
            else
            {
                model.Articles = mapper.Map<IEnumerable<ItemDTO>, IEnumerable<ArticleModel>>(service.LoadNewsFromDatabase(service.GetChannelByUrl(model.SelectedChannel))).ToList();
            }
            if (model.Sort == "Date")
                model.Articles.Sort((x, y) => DateTime.Compare(x.DatePublish, y.DatePublish));
            else
                model.Articles.OrderBy(x => x.Url);
            model.PageInfo = new PageInfo(1, 10, model.Articles.Count);
            return model;
        }


    }
}