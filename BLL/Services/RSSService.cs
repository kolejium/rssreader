﻿using AutoMapper;
using BLL.DTOs;
using BLL.Helpers;
using DAL.Entities;
using DAL.Infrastruct;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class RSSService
    {
        UnitOfWork uow;
        IMapper mapper;

        public RSSService()
        {
            uow = new UnitOfWork();
            mapper = AutoMapperConfig.Mapper;
        }

        /// <summary>
        /// Add channel rss news
        /// </summary>
        /// <param name="url">url channel rss</param>
        /// <param name="save">save changes?</param>
        public void AddChannel(string url, bool save = true)
        {
            RSSChannel rss = new RSSChannel(url);
            if (rss.Channel != null && uow.Factory.Get<IChannelRepository>().Find(rss.Channel.Url) == null)
            {
                uow.Factory.Get<IChannelRepository>().Create(mapper.Map<ChannelDTO, Channel>(rss.Channel));
                if (save)
                    uow.Save();
            }
        }

        /// <summary>
        /// Add channels rss news and save
        /// </summary>
        /// <param name="urls">urls channels rss</param>
        public void AddChannel(params string[] urls)
        {
            foreach (var item in urls)
            {
                AddChannel(item, false);
            }
            uow.Save();
        }

        public void AddChannel(ChannelDTO channel, bool save = true)
        {
            if (uow.Factory.Get<IChannelRepository>().Find(channel.Url) == null)
            {
                uow.Factory.Get<IChannelRepository>().Create(mapper.Map<ChannelDTO, Channel>(channel));
                if (save)
                    uow.Save();
            }
        }

        /// <summary>
        /// Add new news to database from all channels in the database
        /// </summary>
        public void AddNewNewsToDatabase()
        {
            foreach (var item in mapper.Map<IEnumerable<Channel>, IEnumerable<ChannelDTO>>(uow.Factory.Get<IChannelRepository>().Get()))
            {
                AddNewNewsToDatabase(item, false);
            }
            uow.Save();
        }

        public void AddNewNewsToDatabase(string url, bool save = true)
        {
            if (RSSChannel.IsRSS(url))
            {
                RSSChannel rss = new RSSChannel(url);
                AddNewNewsToDatabase(rss.Channel, save);
            }
        }

        public void AddNewNewsToDatabase(params string[] urls)
        {
            if (urls.Length == 0)
            {
                AddNewNewsToDatabase();
            }
            else
            {
                foreach (var item in urls)
                {
                    AddNewNewsToDatabase(item, false);
                }
                uow.Save();
            }
        }

        public void AddNewNewsToDatabase(ChannelDTO channel, bool save = true)
        {
            if (uow.Factory.Get<IChannelRepository>().Find(channel.Url) != null)
            {
                foreach (var item in LoadNewsFromChannel(channel))
                {
                    if (uow.Factory.Get<IItemRepository>().Find(item.Title, item.DatePublish) == null)
                    {
                        uow.Factory.Get<IItemRepository>().Create(mapper.Map<ItemDTO, Item>(item));
                    }
                }
                if (save)
                    uow.Save();
            }
            else
            {
                AddChannel(channel, save);
                AddNewNewsToDatabase(channel, save);
            }
        }

        public void AddNewNewsToDatabase(params ChannelDTO[] channels)
        {
            foreach (var item in channels)
            {
                AddNewNewsToDatabase(item, false);
            }
            uow.Save();
        }

        public bool ChannelExists(string url)
        {
            if (uow.Factory.Get<IChannelRepository>().Find(url) != null)
                return true;
            return false;
        }

        public bool ChannelExists(Channel channel)
        {
            return ChannelExists(channel.Url);
        }

        public ICollection<ItemDTO> LoadNewsFromChannel()
        {
            return LoadNewsFromChannel(mapper.Map<IEnumerable<Channel>, IEnumerable<ChannelDTO>>(uow.Factory.Get<IChannelRepository>().Get()).ToArray());
        }

        public ICollection<ItemDTO> LoadNewsFromChannel(string url)
        {
            return LoadNewsFromChannel(new RSSChannel(url));
        }

        public ICollection<ItemDTO> LoadNewsFromChannel(params string[] urls)
        {
            if (urls.Length == 0)
                return LoadNewsFromChannel();
            List<ItemDTO> items = new List<ItemDTO>();
            foreach (var item in urls)
            {
                items.AddRange(LoadNewsFromChannel(item));
            }
            return items;
        }

        public ICollection<ItemDTO> LoadNewsFromChannel(RSSChannel channel)
        {
            channel.LoadItems();
            return channel.Channel.Items;
        }

        public ICollection<ItemDTO> LoadNewsFromChannel(ChannelDTO channel)
        {
            RSSChannel rss = new RSSChannel(channel.Url);
            return LoadNewsFromChannel(rss);
        }

        public ICollection<ItemDTO> LoadNewsFromChannel(params ChannelDTO[] channels)
        {
            List<ItemDTO> items = new List<ItemDTO>();
            foreach (var item in channels)
            {
                items.AddRange(LoadNewsFromChannel(item));
            }
            return items;
        }

        public ICollection<ItemDTO> LoadNewsFromDatabase()
        {
            List<ItemDTO> list = new List<ItemDTO>();
            foreach (ChannelDTO item in mapper.Map<IEnumerable<Channel>, IEnumerable<ChannelDTO>>(uow.Factory.Get<IChannelRepository>().Get()))
            {
                list.AddRange(LoadNewsFromDatabase(item));
            }
            return list;
        }

        public ICollection<ItemDTO> LoadNewsFromDatabase(ChannelDTO channel)
        {
            return mapper.Map<IEnumerable<Item>, IEnumerable<ItemDTO>>(uow.Factory.Get<IItemRepository>().Get(x => x.Channel.Equals(mapper.Map<ChannelDTO, Channel>(channel)))).ToList();
        }

        public ICollection<ItemDTO> LoadNewsFromDatabase(params ChannelDTO[] channels)
        {
            if (channels != null && channels.Length > 0)
            {
                List<ItemDTO> items = new List<ItemDTO>();
                foreach (var item in channels)
                {
                    items.AddRange(LoadNewsFromDatabase(item));
                }
                return items;
            }
            return LoadNewsFromDatabase();
        }

        public IEnumerable<ChannelDTO> GetChannels()
        {
            return mapper.Map<IEnumerable<Channel>, IEnumerable<ChannelDTO>>(uow.Factory.Get<IChannelRepository>().Get());
        }

        public int CountReadNewsByChannel(ChannelDTO channel)
        {
            return uow.Factory.Get<IItemRepository>().Get(x => x.Read == true && x.Channel.Equals(mapper.Map<ChannelDTO, Channel>(channel))).Count();
        }

        public int CountNewsByChannel(ChannelDTO channel)
        {
            return uow.Factory.Get<IItemRepository>().Get(x => x.Channel.Equals(mapper.Map<ChannelDTO, Channel>(channel))).Count();
        }

        public ChannelDTO GetChannelByUrl(string url)
        {
            if (uow.Factory.Get<IChannelRepository>().Find(url) != null)
                return mapper.Map<Channel, ChannelDTO>(uow.Factory.Get<IChannelRepository>().Find(url));
            return null;
        }

        public string GetStatsByChannel(string url)
        {
            if (uow.Factory.Get<IChannelRepository>().Find(url) != null)
                return GetStatsByChannel(mapper.Map<Channel, ChannelDTO>(uow.Factory.Get<IChannelRepository>().Find(url)));
            else
                return "Channel not found";
        }

        public string GetStatsByChannel(string[] urls)
        {
            string ret = "";
            if (urls.Length > 0)
            {
                foreach (var item in urls)
                {
                    ret += GetStatsByChannel(item);
                }
            }
            else
                GetStatsChannels().ToList().ForEach(x => ret += x);
            return ret;
        }

        public string GetStatsByChannel(ChannelDTO channel)
        {
            return $"Channel: {channel.Name}, {channel.Url} \n\r Count readed news: {CountReadNewsByChannel(channel)}, count news: {CountNewsByChannel(channel)}\n\r";
        }

        public string GetStatsByChannel(params ChannelDTO[] channels)
        {
            string ret = "";
            foreach (var item in channels)
            {
                ret += GetStatsByChannel(item) + "\n\r";
            }
            return ret;
        }

        public IEnumerable<string> GetStatsChannels()
        {
            List<string> list = new List<string>();
            foreach (var item in mapper.Map<IEnumerable<Channel>, IEnumerable<ChannelDTO>>(uow.Factory.Get<IChannelRepository>().Get()))
            {
                list.Add(GetStatsByChannel(item));
            }
            return list;
        }

        public void ReadAllNewsFromChannel(params string[] urls)
        {
            if (urls.Length > 0)
            {
                foreach (var item in urls)
                {
                    if (ChannelExists(item))
                        ReadAllNewsFromChannel(GetChannelByUrl(item), false);
                }
            }
            else
                ReadAllNewsFromAllChannels();
            uow.Save();
        }

        public void ReadAllNewsFromChannel(ChannelDTO channel, bool save = true)
        {
            mapper.Map<ChannelDTO, Channel>(channel).Items.Where(x => x.Read == false).ToList().ForEach(x =>
            {
                x.Read = true;
                uow.Factory.Get<IItemRepository>().Update(x);
            });
            if (save)
                uow.Save();            
        }

        public void UnreadAllNewsFromChannel(ChannelDTO channel, bool save = true)
        {
            mapper.Map<ChannelDTO, Channel>(channel).Items.Where(x => x.Read == false).ToList().ForEach(x =>
            {
                x.Read = false;
                uow.Factory.Get<IItemRepository>().Update(x);
            });
            if (save)
                uow.Save();
        }

        public void ReadAllNewsFromChannel(params ChannelDTO[] channels)
        {
            foreach (var item in channels)
            {
                ReadAllNewsFromChannel(item, false);
            }
            uow.Save();
        }

        public void ReadAllNewsFromAllChannels()
        {
            uow.Factory.Get<IChannelRepository>().Get().ToList().ForEach(x => ReadAllNewsFromChannel(mapper.Map<Channel, ChannelDTO>(x)));
        }

        public void UnreadAllNewsFromAllChannels()
        {
            uow.Factory.Get<IChannelRepository>().Get().ToList().ForEach(x => UnreadAllNewsFromChannel(mapper.Map<Channel, ChannelDTO>(x)));
        }

        public void RemoveChannel(string url, bool save = true)
        {
            if (uow.Factory.Get<IChannelRepository>().Find(url) != null)
            {
                uow.Factory.Get<IChannelRepository>().Remove(uow.Factory.Get<IChannelRepository>().Find(url));
                if (save)
                    uow.Save();
            }
        }

        public void RemoveChannel(params string[] urls)
        {
            foreach (var item in urls)
            {
                RemoveChannel(item, false);
            }
            uow.Save();
        }

        public void RemoveChannel(ChannelDTO channel, bool save = true)
        {
            if (uow.Factory.Get<IChannelRepository>().Find(channel.Url) != null)
            {
                uow.Factory.Get<IChannelRepository>().Remove(mapper.Map<ChannelDTO, Channel>(channel));
                if (save)
                    uow.Save();
            }
        }

        public void RemoveChannel(params ChannelDTO[] channels)
        {
            foreach (var item in channels)
            {
                RemoveChannel(item, false);
            }
            uow.Save();
        }

        private bool IsInDatabase(Channel channel)
        {
            return uow.Factory.Get<IChannelRepository>().Find(channel.Url) != null;
        }

        public void ToDatabase(params RSSItem[] args)
        {

        }


    }
}
