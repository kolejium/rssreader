﻿using AutoMapper;
using BLL.DTOs;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Helpers
{
    public class AutoMapperConfig
    {
        private static Mapper mapper;
        public static Mapper Mapper
        {
            get
            {
                Config();
                return new Mapper(Configuration);
            }
        }

        public static MapperConfiguration Configuration { get; set; }

        static AutoMapperConfig()
        {
            Config();
        }

        private static void Config()
        {
            Configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ChannelDTO, Channel>();
                cfg.CreateMap<Channel, ChannelDTO>();
                cfg.CreateMap<ItemDTO, Item>();
                cfg.CreateMap<Item, ItemDTO>();
            });
        }
    }
}
