﻿using BLL.DTOs;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace BLL.Helpers
{
    public class RSSItem
    {
        private ItemDTO item;

        public ItemDTO Item => item;

        public RSSItem(XmlNode node, ChannelDTO channel)
        {
            item = new ItemDTO();
            item.Channel = channel;
            SetFields(node);
        }

        private void SetFields(XmlNode node)
        {
            foreach (XmlNode x in node.ChildNodes)
            {
                switch (x.Name)
                {
                    case "title":
                        item.Title = x.InnerText;
                        break;
                    case "pubDate":
                        item.DatePublish = Convert.ToDateTime(x.InnerText);
                        break;
                    case "description":
                        item.Description = x.InnerText;
                        break;
                    case "link":
                        item.Url = x.InnerText;
                        break;
                }
            }
        }
    }
}
