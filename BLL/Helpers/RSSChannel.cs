﻿using BLL.DTOs;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;

namespace BLL.Helpers
{
    public class RSSChannel
    {
        private ChannelDTO channel;
        private XmlDocument doc;

        protected XmlDocument Doc => doc ?? LoadXml(channel?.Url);

        public ChannelDTO Channel => channel;

        public RSSChannel(string url)
        {
            if (IsRSS(url))
            {
                channel = new ChannelDTO() { Url = url, Items = new List<ItemDTO>() };
                GetChannel(url);
            }
        }

        public void LoadItems()
        {
            channel.Items = GetItems(Doc.DocumentElement.SelectNodes("channel")[0]).ToList();
        }

        private void GetChannel(string url)
        {
            XmlNode channelNode = Doc.DocumentElement.SelectNodes("channel")[0];
            SetFields(channelNode);
        }

        private ICollection<ItemDTO> GetItems(XmlNode channelNode)
        {
            XmlNodeList xmlNodes = channelNode.SelectNodes("item");
            List<ItemDTO> items = new List<ItemDTO>();
            foreach (XmlNode item in xmlNodes)
            {
                RSSItem rssItem = new RSSItem(item, Channel);
                items.Add(rssItem.Item);
            }
            return items;
        }

        private void SetFields(XmlNode channelNode)
        {
            XmlNode item = channelNode.SelectNodes("title")[0];
            channel.Name = item.InnerText;
        }

        public static bool IsRSS(string url)
        {
            if (LoadXml(url).DocumentElement.Name == "rss")
                return true;
            return false;
        }

        private static XmlDocument LoadXml(string url)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(url);
                return doc;
            }
            catch (Exception x) // даже если это не юрл
            {
                return BadLoadXml(url);
            }
        }

        private static XmlDocument BadLoadXml(string url)
        {
            string xml = string.Empty;
            
            XmlDocument doc = new XmlDocument();
            using (WebClient wc = new WebClient())
            {
                xml = wc.DownloadString(url);
            }
            xml = xml.Replace("\n", "");
            doc.LoadXml(xml);
                //using (TextReader reader = XmlReader.Create(url, new XmlReaderSettings() { IgnoreComments = true, IgnoreWhitespace = true }))
                //{
                //    string temp = string.Empty;
                //    while ((temp = reader.read.ReadLine()) != null)
                //        if (temp != string.Empty)
                //            xml += temp;
                //    doc.LoadXml(xml);
                //}
                return doc;
        }
    }
}
