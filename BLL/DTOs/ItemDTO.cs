﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTOs
{
    public class ItemDTO
    {
        public string Title { get; set; }
        public DateTime DatePublish { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }

        public bool Read { get; set; }

        public ChannelDTO Channel { get; set; }

        public override string ToString()
        {
            return $"\n\r{Title} | {DatePublish} | \n\r{Description}\n\r{Url} - {Read}\n\r Source: {Channel.Url}";
        }
    }
}
