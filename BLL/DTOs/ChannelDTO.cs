﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTOs
{
    public class ChannelDTO
    {
        public string Url { get; set; }
        public string Name { get; set; }

        public List<ItemDTO> Items { get; set; }

        public override string ToString()
        {
            return $"{Name}, ({Url})";
        }
    }
}
